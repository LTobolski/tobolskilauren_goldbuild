//
//  InterfaceController.swift
//  TobolskiLauren_Wearable_Project WatchKit Extension
//
//  Created by Lauren Tobolski on 5/7/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import WatchKit
import Foundation
import CoreMotion
import WatchConnectivity


class InterfaceController: WKInterfaceController {

    //Declaring my variables
    let session = WCSession.default
    let motionManager = CMMotionManager()
    var previousZ :Double?
    var lastLapTime = Date()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        //So the acceloremeter will activate when the user fils their wrist but this will only happen after 5 seconds to make sure this isn't an instant movement.
        motionManager.accelerometerUpdateInterval = 0.5
//        startRecording()
        self.session.activate()
        self.session.delegate = self
    }
    
    @IBAction func startRace() {
//        if WCSession.isSupported() {
//
//            if self.session.isReachable {
//                try? session.updateApplicationContext(["Hello" : "There"]);
//            }
//        }
    }

}

//Testing purposes
//If user flips wrist the log will appear 
extension InterfaceController: WCSessionDelegate {
    
    func session(_ session: WCSession, didReceiveMessageData messageData: Data) {
        print("Message received: ",messageData)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Message received: ",message)
    }
    
    func session(_ session: WCSession, didFinish userInfoTransfer: WCSessionUserInfoTransfer, error: Error?) {
        print(error?.localizedDescription)
    }
    
    
    //below 3 functions are needed to be able to connect to several Watches
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    

    
}
