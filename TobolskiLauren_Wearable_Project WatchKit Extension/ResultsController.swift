//
//  ResultsController.swift
//  TobolskiLauren_Wearable_Project WatchKit Extension
//
//  Created by Lauren Tobolski on 5/24/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import WatchKit
import HealthKit
import WatchConnectivity


class ResultsController: WKInterfaceController {
    
    @IBOutlet var table: WKInterfaceTable!
    @IBOutlet var heartRateLabel: WKInterfaceLabel!
    
    //Variables
    var laps = [Double]()
    
    let session = WCSession.default
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if let thelaps = context as? [Double] {
            laps = thelaps
        }
        //Setting the number of rows
        table.setNumberOfRows(laps.count, withRowType: "Lap")
        for index in 0..<table.numberOfRows {
            //Row Controller
            guard let controller = table.rowController(at: index) as? LapRowController else {continue}
            let lap = laps[index]
            //Setting text
            controller.lapCountLabel.setText("\(index + 1)")
            controller.timeLabel.setText("\(lap)")
        }
        
        //Heart rate monitor - setting the text
        let heartRate = HeartRateMonitor.shared.getAverageHeartRate()
         self.heartRateLabel.setText("\(heartRate)")
        
        self.session.activate()
        self.session.delegate = self
        
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didAppear() {
        super.didAppear()
    }
    
    //Dismiss the view
    @IBAction func dismissView() {
        saveRaceData()
        popToRootController()
    }
    
    //Save race data method
    func saveRaceData() {
        //getting the race info , so this will set the heart rate and the laps 
        let raceInfo: [String : Any] = ["heartRate" : 80, "laps" : laps];
        if WCSession.isSupported() {
            if self.session.isReachable {
                try? session.updateApplicationContext(raceInfo);
            }
        }
    }
    

}

extension ResultsController: WCSessionDelegate {
    
    func session(_ session: WCSession, didReceiveMessageData messageData: Data) {
        print("Message received: ",messageData)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Message received: ",message)
    }
    
    func session(_ session: WCSession, didFinish userInfoTransfer: WCSessionUserInfoTransfer, error: Error?) {
        print(error?.localizedDescription)
    }
    
    
    //below 3 functions are needed to be able to connect to several Watches
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    
    
    
}



