//
//  LapRowController.swift
//  TobolskiLauren_Wearable_Project WatchKit Extension
//
//  Created by Lauren Tobolski on 5/24/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import WatchKit

class LapRowController: NSObject {
    
    @IBOutlet var timeLabel: WKInterfaceLabel!
    @IBOutlet var lapCountLabel: WKInterfaceLabel!

}
