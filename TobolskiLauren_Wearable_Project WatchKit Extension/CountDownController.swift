//
//  CountDownController.swift
//  TobolskiLauren_Wearable_Project WatchKit Extension
//
//  Created by Lauren Tobolski on 5/23/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import WatchKit
import CoreMotion

class CountDownController: WKInterfaceController {
    
    @IBOutlet var timer: WKInterfaceTimer!
    @IBOutlet var lapLabel: WKInterfaceLabel!
    //Variables
    var lapCounter  = 0
    var lastDate: Date?
    let minimumLapChangeThreshold: Double = 5
    
    let motionManager = CMMotionManager()
    var previousZ :Double?
    
    var lapTimes = [Double]()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        //My accelorometer count down
        motionManager.accelerometerUpdateInterval = 0.5
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    //When view appears
    override func didAppear() {
        super.didAppear()
        restartLapTimer()
        startRecording()
        HeartRateMonitor.shared.startDate = Date()
        HeartRateMonitor.shared.subscribeToHeartBeatChanges()
    }
    //Function to restart my lap timer
    func restartLapTimer() {
        //First updating the lap times
        updateLapTimes()
        //Setting date
        timer.setDate(Date())
        lastDate = Date()
        timer.start()
        //If lap counter is greater than 5 end the race
        if lapCounter >= 5 {
            lapCounter = 0
            endRace()
            return
        }
        lapCounter += 1
        lapLabel.setText("Lap \(lapCounter)")
    }
    
    //Start recording function
    func startRecording() {
        //The accelerometer will start
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
            if let previous = self.previousZ, let current = data?.acceleration.z {
                if abs(current - previous) >= 1 {
                    print("wrist Raise")
                    //If wrist raise it will start a new lap
                    if self.canStartNewLap() {
                        self.restartLapTimer()
                    }
                }
            }
            self.previousZ = data?.acceleration.z
        }
    }
    //End race method
    func endRace() {
        print("End race")
        pushController(withName: "Results", context: lapTimes)        
    }
    
    //Update lap times
    func updateLapTimes() {
        //If lapcounter is greater than 0 it will increase
        if lapCounter > 0 {
            lapTimes.append(timeIntervalSinceLastLap())
            debugPrint(lapTimes)
        }
    }
    
    //So this function will continue off from the last lap
    //It will restart
    func timeIntervalSinceLastLap() -> Double {
        if let theLastDate = lastDate {
            return Date().timeIntervalSince(theLastDate).roundTo(places: 2)
        }
        return 0
    }
    
    //Start new lap method
    func canStartNewLap() -> Bool {
        if timeIntervalSinceLastLap() < minimumLapChangeThreshold {
            return false
        }
        return true
    }
}
 //extension for timer 
extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
