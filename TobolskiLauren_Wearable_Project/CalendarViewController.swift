//
//  CalendarViewController.swift
//  TobolskiLauren_Wearable_Project
//
//  Created by Lauren Tobolski on 5/12/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //viewWillAppear 
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
}
//I had to set an extension for the calendar
//If user clicks on calendar it will go to the laps view controller 
extension CalendarViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if let lapsVC = storyboard?.instantiateViewController(withIdentifier: "Laps") as? LapsViewController {
            lapsVC.date = date
            navigationController?.pushViewController(lapsVC, animated: true)
        }
    }
}
