//
//  LapCell.swift
//  TobolskiLauren_Wearable_Project
//
//  Created by Lauren Tobolski on 5/12/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import UIKit

class LapCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lapNumberLabel: UILabel!
    @IBOutlet weak var lapTimeLabel: UILabel!
    
    //This is the lap cell and how the layout is set up
    override func layoutSubviews() {
        super.layoutSubviews()
        //Container view 
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
    }

}
