//
//  LapsViewController.swift
//  TobolskiLauren_Wearable_Project
//
//  Created by Lauren Tobolski on 5/12/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import UIKit

class LapsViewController: UIViewController {

    
    @IBOutlet weak var bestLapTimeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //Laps and date variable to store that data in
    var date: Date?
    var laps = [Double]()
    
    //When the view loads
    //This will display the data after the racer has finished
    //Fetching laps info method
    override func viewDidLoad() {
        super.viewDidLoad()
        displayDate()
        fetchLapsInfo()
    }
    
    //Method for laps info
    func fetchLapsInfo() {
        guard let theDate = date else {
            return
        }
        //Getting date
        //Date formatter
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        //Getting laps
        if let raceInfo = UserDefaults.standard.dictionary(forKey: formatter.string(from: theDate)) {
            if let theLaps = raceInfo["laps"] as? [Double] {
                self.laps = theLaps
                self.tableView.reloadData()
            }
        }
        //I have to retrive the best lap also
        var bestLap: Double = 0
        for lap in laps {
            if bestLap == 0 {
                bestLap = lap
            } else if lap < bestLap {
                bestLap = lap
            }
        }
        bestLapTimeLabel.text = "\(bestLap)"
    }
    
    //This will display the date
    func displayDate() {
        guard let theDate = date else {
            return
        }
        //Date Formatter
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM ddth, yyyy"
        title = formatter.string(from: theDate)
    }

}

//This will return the number of laps in the table view
//Display the table rows
extension LapsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laps.count
    }
    
    //This will fill up the table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Lap", for: indexPath) as! LapCell
        let lap = laps[indexPath.row]
        //Filling up the lap number
        cell.lapNumberLabel.text = "\(indexPath.row + 1)"
        //Fill lap time
        cell.lapTimeLabel.text = "\(lap)"
        //Have to return cell 
        return cell
    }
}
