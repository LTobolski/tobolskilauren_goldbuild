//
//  ViewController.swift
//  TobolskiLauren_Wearable_Project
//
//  Created by Lauren Tobolski on 5/7/18.
//  Copyright © 2018 Lauren Tobolski. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    //When loading the notification center will activate
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showAlert), name: Notification.Name.init("Watch"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Navigation bar will be hidden
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //Testing a fake alert
    @objc func showAlert() {
        let alert = UIAlertController(title: "klgjadsg", message: "gkaljdglka", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "kldgja", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
  


}

extension Double {
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

